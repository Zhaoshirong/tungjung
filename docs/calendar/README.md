# 2025 MEETINGS AND EVENTS

New Year's Dinner Dragon's Restaurant Sunday 2nd Feb 2025 6 pm 

> Cost Non-Members: $55.00 pp or $550pp per table of 10.
> Tung Jung **current members** subsided fee is $40 pp or $400 per table of 10 
> 
> Cash or Internet Banking to Account 01-0505-0178453-00 Ref. CNY
> Early booking is essential, as tables are limited.
> RSVP by Sunday, 26 January 2025
> To Peter Moon at peteraumoon@yahoo.co.nz 
> or MB 021 02253097
> 
> If there are late bookings these may be considered depending on any spaces available

## Menu for CNY Dinner

> CHINESE NEW YEAR BANQUET
> 
> Chicken Herbal Soup
> Celestial Bloom Cordyceps Chicken Essence
> a nourishing fusion of chicken and cordyceps militaris, enriched with delicate floral notes.
> 
> MENU
> Cantonese White Cut Chicken
> Tasty & tender
> 
> Crispy Belly Roast Pork
> Salty, meaty, and it's hearty.
> 
> Cantonese Roast Duck
> Moist, savoury, and succulent
> 
> Steamed Blue Cod  
> Mild sweet & tantalizing taste
> 
> Stir-Fry Shrimp with Broccoli
> Subtly sweet flavour
> 
>  Gai Lan Stir Fry with Shiitake Mushroom
> Earthy, smoky & tender
> 
>  Dragon Fish Patties with Vermicelli
> ** (Chef Favourite) **
> 
> Dragon Long Life Noodles with BBQ Pork, mushroom & carrots
> Chewy texture and mild savoury flavour
> 
>  Beverage
> Wine BYO * Orange Juice * Jasmine Tea*
> 
> Jasmine Rice
> Unique aroma, flavour, and texture
> 
> Dessert
> Mango Fish Pudding
> Sweet, Smooth & simply delicious
> 
> Fresh Fruit Platter
> 
> Raffles: 3 tickets for $5
> Over $400 worth of prizes

# SOCIAL 2025 (Year of the Wood Snake)

- Ching Ming 2025 April 6, Sunday 12 pm [Tung Jung Memorial Stele](/genealogy/karori-cemetery.md)

- Mid-Winter Yum Cha 2025 June 4, Sunday 12 pm at TBA  $TBA pp for members, $TBA pp non-members. 

- Moon Festival (中秋節) 2025 October 12, Sunday location and time TBA. $TBA pp for members, and $TBA for non-members. Please book tables with [Peter Moon](https://tungjung.nz/contact/#social-committee)

- [Chung Yeung](/genealogy/karori-cemetery.md)  2025 November 2nd Sunday 12 pm (9th day of the 9th month of the Lunar Calendar)

- Christmas Seniors' Yum Cha 2025 December 3rd, Wednesday 2025 TBA

# 2024 MEETINGS AND EVENTS

New Year's Dinner Dragon's Restaurant Sunday 18 Feb, 2024 6 pm $50 pp

- AGM 18th August 2024 2pm at the [Johnsonville Collective Community Hub](https://www.collectivehub.co.nz).  This will be folowed by a hot pot dinner for those attending the AGM at the [Babaili Restaurant](https://www.babaili.co.nz/babaili-hot-pot)

## Executive Committee Meetings

Executive Committee meetings usually start at 7:30 pm on the first Monday of each month, commencing 6th Feb 2023

# Social

- Ching Ming Sunday 7th April, 2024 12 pm [Tung Jung Memorial Stele](/genealogy/karori-cemetery.md)

- Mid-Winter Yum Cha 5th June, 2024 12 pm at Dragon's Restaurant.  $15 pp for members, $25 pp non-members. 

- Moon Festival (中秋節) 22nd September, 2024.  6 pm Dragon's Restaurant. $45 pp for members, and $50 for non-members. Please book tables with [Peter Moon](https://tungjung.nz/contact/#social-committee)

- [Chung Yeung](/genealogy/karori-cemetery.md) Sunday 13th October, 2024 (9th day of the 9th month of the Lunar Calendar)

- Christmas Seniors' Yum Cha Wednesday 11th December, 2024

