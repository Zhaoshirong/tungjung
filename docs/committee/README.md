# The Executive Committee for 2024 - 2025
 

| Office         | Name                         |
|-------------------|--------------------------------------|
| President         | Graham Chiu 趙世榮                        |
| Past president    | Kevin Leong 梁永基                              |
| Vice President    | Rayward Chung 鍾振威                       |
| Treasurer         | Lucinda Chiu 趙潔蘭 |
| English Secretary | Helen South 楊雪梅  |
| Chinese Secretary | Kevin Zeng 曾凯文      |

## Subcommittees:

| Committee         | Name                         |
|-------------------|--------------------------------------|
| Property          | Thomas Chong 吳永華|
| Membership	 	| committee to appoint |
| Social	 	    | **Peter Moon 歐偉權**, Valerie Ting 陳惠娌 |
| Communication	 	| Peter Moon 歐偉權 |
| Website	    	| Graham Chiu 趙世榮 |
| Public relations 	| Vacant |
| Club History	 	| committee to appoint |
| Newsletter	 	| Graham Chiu 趙世榮 |

## Wellington Chinese Association

Tung Jung Representative: Kevin Leong (2023-2024)

## Ventnor

Murray Wu - representing Wu family and Tung Jung Association

## Video Conferencing

We have Google Hangouts MEET available to us which allows a group of up to 250 to meet.  This facility is provided free by Google until July, and hopefully extended if the crisis extends past that.

You need to login using your tungjung email address as that means as a part of the TungJung domain the person who starts the meeting doesn't have to let you into the meeting and it is sometimes not apparent that someone is waiting to be let in the room. So this is a manual process required for those logging in using their xtra, gmail or other identities.

Download the [Meet App for Android](https://play.google.com/store/apps/details?id=com.google.android.apps.meetings&hl=en) for Android users.  

And on iPhone [Meet App for Apple](https://apps.apple.com/us/app/hangouts-meet-by-google/id1013231476)

If you don't have a smart phone you can use a browser, and you'll receive a link to join the meeting.

## Committee Expenses

Please use this [claim form](assets/Tung_Jung_NZ_Expense_Claim_form.docx) for committee expenses

