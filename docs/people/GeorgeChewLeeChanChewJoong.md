# George Chew Lee/Chan Chew Joong

Born in Tarp Gwong village in 1877. He came to New Zealand in 1896 and moved to Feilding in 1904 where he opened a fruit shop. In 1913 he moved to Christchurch, opening another fruit shop. He was a founder member and first President of the Christchurch Kuomintang, staunch supporter of the Tung Jung Association and a leader of the Christchurch Chinese community.