# People

## Chan Yin/Chan Check Yin

Chan Yin was born in Ha Gee village in 1870, arrived in Wellington in 1896, and was naturalised in 1905.

[read more](./Chan-Yin-Chan-Check-Yin.md)

## Bickleen Wang née Ng

Bickleen Ng was the first Chinese woman to get a post-graduate degree in New Zealand, obtaining an MA in Education for her 1955 study of the assimilation of Chinese in New Zealand.

[read more](./BickleenWang.md)

## George Chew Lee/Chan Chew Joong

Born in Tarp Gwong village in 1877.

[read more](./GeorgeChewLeeChanChewJoong.md)

## Chan Hock-joe/Joe Ah Chan and Yip Kue-sum

Chan Hock-joe (known in New Zealand as Joe Ah Chan) was born in Ha Gee village in 1882 and arrived in New Zealand around 1905.

[read more](./ChanHock-joeJoeAhChanandYipKue-sum.md)

## Wong Sik She

Wong Sik She was born in 1863 in Gwa Leng village.

[read more](./WongSikShe.md)

## William 'Bill' Wong

Bill Wong's father Wong Cho Ling was born in Sha Tau village in 1871 and came to New Zealand in 1894.

[read more](./WilliamBillWong.md)

## William & Wallingford Chan

William Yau-kum Chan was born in 1870 in Sun Gai village.

[read more](./WilliamAndWallingfordChan.md)

## George Gee/Luey

George Gee was born in Palmerston North in 1921, the son of Louis Choi Gee, a Bark Shek man who was born in 1881 and had emigrated to New Zealand in 1906, and Wong Wing-ling.

[read more](./GeorgeGeeLuey.md)

## Chun Dun/Chan Tsaan Tang

Chan Dun was born in 1860 in Sha Chuen village and arrived in Otago in 1872.

[read more](./ChunDunChanTsaanTang.md)

## Chan Dar-chee/Ah Chee

Born Tarp Gwong village in 1851.

[read more](./ChanDarcheeAhChee.md)

## Louis Tung Kitt

Born in Bark Shek in 1869.

[read more](./LouisTungKitt.md)

## William Wong Tong/Wong Tong Fat

Born in Gwa Leng village in 1865, Wong Tong Fat arrived in Wellington in 1893 and opened a fruitshop in Cuba Street.

[read more](./WilliamWongTongWongTongFat.md)

## William Kwok/Kwok Kee-yee

Born in Bark Shek village in 1885, the son of one three brothers who emigrated to New Zealand between 1892 and 1896.

[read more](./WilliamKwokKwokKee-yee.md)

## Chin Ting/Chan Moon-ting/James Chin Ting

Chin Ting was born in Sun Gai village in 1859, one of three brothers who emigrated to New Zealand.

[read more](./ChinTingChanMoontingJamesChinTing.md)

## Wong Kwok-min

Born in Gwa Leng village in 1874 and arrived in New Zealand in 1892 , initially settling in Greymouth, where he opened a general store.

[read more](./WongKwok-min.md)
