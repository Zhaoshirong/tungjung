# William Kwok/Kwok Kee-yee

![](assets/Kwok-Historic_0662.jpg)

Born in Bark Shek village in 1885, the son of one three brothers who emigrated to New Zealand between 1892 and 1896. He arrived in New Zealand in 1901, eventually settling in Wellington. He was a good businessman and community leader, being dedicated to the welfare of the Chinese New Zealand community. He was a founder member of the Tung Jung Association in 1925 and was the Association's longest serving President, a position he held between 1929 and 1948. He was also a dominant figure in the New Zealand Kuomintang and the New Zealand Chinese Association. William Kwok died in 1975.
