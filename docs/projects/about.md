# Business Case for Funding a Community Hub for the Local Wellington Chinese Community

## Executive Summary
The establishment of a **Community Hub** for the local Chinese community aims to foster cultural integration, provide social support, and promote economic development. This hub will serve as a safe space for community gatherings, educational programs, and business networking opportunities, thereby enhancing the overall well-being of its members.

## Background
The local Chinese community has been growing steadily over the past few years both from natural expansion but also immigration from China, and the countries that host the Chinese diaspora. Various groups that have been here over the last 100+ years have built their own communities but their facilities are generally restricted to their own memberships and not necessarily suitable. However, there is a lack of dedicated spaces and resources that cater specifically to join together the new migrants with the established original Chinese communities. A Central City Community Hub will address this gap, bringing together individuals and families to celebrate their culture, share experiences, and support one another.  The Old Chinese Mission Hall has deep historical connections to the local Chinese Community, and has a unique potential to revive those nurturing traditions.  the Old Chinese Mission Hall is currently being strengthened and refurbished as part of the Resource consent granted for the building of the adjacent housing structure.  It just requires a suitable tenant for this work to be completed,

## Objectives
The primary objectives of the Community Hub include:

- **Cultural Preservation**: Provide a venue for cultural events, festivals, and workshops (calligraphy, water colours, bonsai, meditation).
- **Education**: Offer language classes, tutoring, and vocational training.
- **Social Support**: Create a network for newcomers and existing members to share resources and information.
- **Economic Development**: Facilitate business networking and support local entrepreneurs.

## Proposed Services
The Community Hub will offer a variety of services:

- **Cultural Events**:
  - Festivals (e.g., Chinese New Year celebrations, Moon Festival)
  - Art exhibitions and performances
- **Educational Programs**:
  - Language courses (Mandarin, Cantonese)
  - Workshops on Chinese traditions and customs
  - Cooking classes
- **Recreational Facilities**:
  - Chinese games
  - Calligraphy
  - Painting
- **Support Services**:
  - Drop in Centre
  - Counseling for newcomers
  - Legal and financial advice

## Target Audience
The primary beneficiaries of the Community Hub will include:

- Local Chinese families and individuals
- New immigrants seeking support and integration
- Local businesses aiming to connect with the community

## Financial Projections
### Funding Requirements
To establish the Community Hub, an estimated funding of **$40,000 pa** is required for:

- **Facility Lease**: **$30,000**
- **Operational Costs**: **$10,000**

### Revenue Generation
The Hub will generate revenue through:

- Event ticket sales
- Grants and sponsorships with majority underwriting by the three Cantonese district Associations (Poon Fah, Seyip, Tung Jung)
- Rental of facilities for private and community events
- Possible Tea House operating during business hours

### Financial Sustainability
The Community Hub is likely to need sustained funding from the Cantonese Associations and perhaps Poll Tax Heritage Trust

## Impact Assessment
The Community Hub is expected to have a positive impact on:

- Community cohesion and cultural pride
- Educational attainment and skills development
- Local economic growth through business support
- Spread awareness of the Chinese Community as an integrated part of NZ society

## Conclusion
Funding the Community Hub for the local Chinese community represents an investment in the cultural and economic future of the area. By providing a dedicated space for cultural expression, education, and business development, this initiative will strengthen the community and promote inclusivity. 

We respectfully request your support in making this vision a reality.