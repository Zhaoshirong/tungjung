# Tung Jung Obituaries ( Work in progress )

These obituaries have been submitted by family members for publication.
If you wish to have your family member's obituaries published, please contact secretary@tungjung.nz

## Chung, Howard, 鐘振揚 Tai Chuen Village 泰村 of 增城增城   4 July 1940 – 16 November 2011 

See https://tungjung.nz/newsletters/assets/Autumn_issue_2012.pdf

## Hoon, Bill 沈 錫 耀 of Jurng-dai  village 張大村 5 July 1943—2 May 2012 

See https://tungjung.nz/newsletters/assets/Winter_issue_2012.pdf

## Kee-Sue, Barry Vincent 18 January 1946 - 24 Jan 2024

Barry (grandson of 黎岐瑞) passed on 24 January 2024 aged 78. He left this country for Sydney in 1982, but was always a
proud Pātea boy, and left sincere messages on the Pātea Historical Society facebook page when we
put up stories and photos from the past.

[more ...](./barry-kee-sue.md)

## Kwok Goddard, Nancy Wai-Lan 郭蕙蘭 of Bak Shek Village 白石村 1923 – 10 September 2012 

See https://tungjung.nz/newsletters/assets/Summer_issue_2012_amended.pdf

## Ng, George Gee Leong 吳志良  -   Nga Yiel village 雅瑤村 29 December 1929 – 15 July 2012 

See https://tungjung.nz/newsletters/assets/Spring_2012_issue.pdf

## Tso, Shirley 陳 16 July 1926 – 14 December 2019

See https://tungjung.nz/newsletters/assets/Autumn_2020_issue.pdf

## WONG, Jim 黃顯東 of BAK SHEK VILLAGE 白石村  7  AUGUST 1929 - 11 SEPTEMBER 2012 

See https://tungjung.nz/newsletters/assets/Summer_issue_2012_amended.pdf

## Wu, Gordon 10 Feb 1938—12 Feb 2024

See https://tungjung.nz/newsletters/assets/Autumn_2024_issue.pdf
