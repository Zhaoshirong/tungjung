---
# This is the header page written in YAML
home: true
heroImage: /tungjung.png # https://www.tungjung.org.nz/images/tungjung-logo-large.png
heroText: Tung Jung Association of NZ Inc (1926)
tagline: 新 西 蘭 東 增 會 館
actionText: 開門 Enter →
actionLink: /about/
features:
- title: Cultural
  details: Promote the culture of the peoples from the Tung Gwoon (Donguan) and Jungsen (Zengcheng) districts of China
- title: Education
  details: Teach the spoken Guangdong dialect of China, and Chinese literacy
- title: Social
  details: Organise social events for members and celebrate Chinese customs
footer: Copyright © 2020-present Tung Jung Association of NZ Inc, Hosted on GitLab.io
# You need to edit this file to see the special announcements
# which come from components stored in /docs/.vuepress/components
---



