# Wellington Mahjong & Chinese Games Club

<img src="/games/assets/Games_logo.jpg" alt="logo" width="300" align="center"/>

**Club Details**

- Monthly Saturday afternoon, 1 - 5.30pm (drop in anytime)
- Wellington Bridge Club - Moturoa Room (Level 1), [17 Tinakori Rd, Thorndon](./wbc.md) 
- $10 per person
- Tea, coffee and light afternoon tea provided.

**Sign up to the next session**: [Google Forms Signup](https://forms.gle/CLD9GKkaSNLneJnQ8)


**Calling all Mahjong (麻將), Chinese Chess (象棋) and Go (圍棋) players**

Whether you're a complete beginner or have some skills under your belt, come along to enjoy some friendly games at our Saturday afternoon club sessions. 

Drop in anytime, for a few hours or more. Bring along friends and family.

**Game sets**: Please bring along your Mahjong, Chinese Chess and Go sets in case they are needed!

**Payment options**: 
Cash, Internet banking, or Credit card with surcharge.

Internet banking: 
- Lucinda Chiu *02-0500-0677990-003*
- Particulars: *Your name* 
- Reference: *Event date or month(s)*

Credit card:
- Surcharge applies (60c/ticket)
- Payable on the day or online on Humanitix: https://events.humanitix.com/host/wgtn-mahjong-and-chinese-chess-club

Contact: Lucinda <treasurer@tungjung.nz> for more information.

**Upcoming Sessions**:

**Saturday afternoon**:
- 15 Feb 2025 (held with Wellington China Tongue)
- 22 Mar 2025 
- 12 Apr 2025 
- 17 May 2025 (held with Wellington China Tongue)
- 14 Jun 2025 

_Joint sessions with WCT will be held in the Pipitea Room on the ground floor._

**Friday evening**:

These will be scheduled occassionally. Limited spaces will be available with prepayment required to secure your spot. 
* 7 Mar 2025, 6-11pm at $15pp with light dinner and snacks provided

**Venue info**:

- Stairs: There are two flights of stairs to reach the Moturoa Room on Level 1. There are no lifts.
- Accessibility: There is an outdoor footpath giving access into the building.
- Parking: Limited Off-street parking is available beneath the building.

_Sponsored by:  
Tung Jung Association of NZ Inc 1926 (新 西 蘭 東 增 會 館)_