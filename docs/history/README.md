# A Brief History of the Tung Jung Association

Life in New Zealand in the 1920s was difficult for the Chinese. There were many prejudices to overcome both socially and in business. In 1926 a group of progressive and far-sighted people from Tung Gwoon and Jungsen counties decided to form an association for mutual help in a country far from home. Thus was born the NZ Tung Jung Association - the first Chinese community organisation to be established in New Zealand. The primary objective of the Association is to unite and maintain the identity and kinship of those who claim affinity to the counties of Tung Gwoon and Jungsen and their descendants.

## Early Years

The Association was formed under the leadership of Mr Chan Ting. Other foundation members included Messrs Young Low On, Wong Tong, Ng Yew Sui, Wong Pang Quai, Luey Shew and others.

A property in Frederick Street was purchased from members' donations and officially opened in 1926 when the Association started helping its members. The building became the focal point of activity where the annual Ching Ming and Chung Yeung dinners were held.


![Frederick Street](images/frederick_street_thumb.jpg)


In 1928 Mr William Kwok was Chairman - position he held until after World War II. During this difficult period great strides were made to consolidate the association in the eyes of its members: A constitution was drawn up and the property was completely renovated (thanks to the efforts of Mr K C Lowe and others).

## Post World War II

At the end of the war, members were naturally concerned about their kinsmen back home and many returned to China to be re-united with their family. A majority came back to NZ bringing with them their family and as a result membership increase. The Association became more active in providing social support services to its members and increasingly became the venue for many social events. In this period, the Association bought cemetery plots and built a memorial. Chairmen included Messrs Lambert Gee, William Chong, Wong Kai Hing and Tom Wong Nam.

## The 1950s and 60s

Notables events included celebration of the Associations 40th Anniversary, the census of Tung Jung people in New Zealand, assistance to 17 Taiwanese fishermen and the purchase of a second cemetery plot. Mr Leslie Wu and Mr James Young were presidents in this period.

## The 1970s and 80s

The Association celebrated its Golden Anniversary and there was a revival of activities. The constitution was re-drafted and the Association became an incorporated society. Mr Harry Young and and Mr James Luey served as Chairmen.

## From 1989 to 1991

During this period there was renewed contact with Jungsen county, fundraising for flood relief in China, major repairs to the property and celebrating the Associations 65th Anniversary, hosting official visitors from Jungsen. Mr Shum W Chong is President in this period.

## 1991 to 2002

The Association sells its building in Frederick Street and buys another in Webb Street. There is an influx of Chinese people from China and Taiwan greatly increasing the total number of Chinese people in New Zealand - however the proportion of "local" Chinese is much diluted. The Association increasingly engages in joint functions with the Seyip Association and Poon Far Association to celebrate Chinese New Year, the Mid-Autumn Festival, and Chinese National Day. The Association seeks active participation by post-war "baby boomers" and new directions.

![Webb Street](images/webb_street_thumb.jpg)

The challenge for the Association in the future is how to retain its links and traditions with the past and yet grow and prosper in the new environment where many of the younger generation have new and different outlets for their energies and interests. How to meet the needs of its members in the future is the immediate and major challenge for the Association to address.


## 2003 - present

The focus of the committee's energies has been on replacing the Webb Street building with another larger and more suitable building on Torrens Terrace. This has been a major exercise. The new building will fulfil the two major goals of "usefulness and suitability" and "asset growth". It comprises of three floors. The top floor has been converted to a five bedroom apartment, the second floor will eventually be a four bedroom apartment and the bottom floor is currently been used for conducting the business of the association. 

The association is gradually defining its goals and objectives. As well as providing support for its members and playing an active role in community activities there is a need to help families with their research into family histories. This is still at an early stage but it is likely that the facilities in the new building will be available to members for this purpose. The association's oral history project, support of Dr Henry Chan's research into the migration of Chinese from Zengcheng to New Zealand along with its recent involvement in providing information for the poll tax exhibition "A Barbarous Measure" all contribute to this overall goal. There is an upsurge in interest into the background and history of the early Chinese in New Zealand. Because of all these activities, the Tung Jung Association is gaining recognition as a leading example for others to follow.


*Abstracted from an article by Jack T H Chung in the 65th Anniversary Book

## National Library of NZ

Many of the documents of the Tung Jung Association have been donated to the National Library.  These include photographs, oral histories, minutes and other miscellaneous materials.

The link is [here](https://natlib.govt.nz/records/22410154)





