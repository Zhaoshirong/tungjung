# Sungai Village

## Pre-amble

This information about Sun-gai village has been taken from the publication **"Sun-gai Village and the New Zealand Connection"** published in conjunction with the 2nd Sun-gai Chan Family Gathering in Auckland, 25th October 1998, compiled and edited by Raymond Chan. 

This family gathering followed an earlier inaugural one in Wellington in 1996.

The Tung Jung Association of New Zealand Inc. gratefully acknowledges the source of this information and hopes that it will generate interest in the roots and history of the early Chinese in New Zealand, especially those who originated from the Jung-sing County.  The association hopes that this information will encourage others to write about their villages. 

## Cantonese Migration to New Zealand

Guangdong (Kwangtang) is a province of South China.  Virtually all of the early migration (late 19th/early20th Centuries) from China to New Zealand came from a number of Guangdong counties, mainly Jung-sing (also known as Tsengshing, Zengcheng, or Jung-sen), Se-yip and Poon-yue.  China was in the grip of anarchy, banditry, poverty and overcrowding.  The gold rush of North America was first heard about in China, in Guangdong.  This represented a "golden" opportunity to escape poverty.  America became known as the "gum san" (gold mountain).  The later gold rushes of Australia, then New Zealand, represented the new "gum san".

The earliest sojourners became gold miners in the countryside.  They were virtually all male in that they intended to return to China.

As the gold ran out, the Chinese moved to the cities, turning to independent businesses such as fruiterers, laundry and market gardening.  Merchants such as Wah Jangs in Auckland and Yee Chong & Wing in Wellington often provided security and jobs for later migrants until they could establish themselves.

There was a huge anti-Chinese (Yellow Peril) sentiment in New Zealand during the turn of the century.  This was legislated via Immigrant (read Chinese) Restriction Acts, and other barriers to Chinese migration.

The local Chinese community remained "marginalized" and never really took on the essence of a community until the Second World War when China became allied to the "Allied" countries.  New Zealand eased its anti-Chinese stance and allowed 249 wives and 244 children of Chinese men in New Zealand to come here as war refugees.  It was because of the protracted and ongoing Sino-Japanese wars and a changing sentiment towards Chinese that they were later granted residency status.  A Chinese family community was fortuitously seeded.  These men and their families were not sojourners anymore; they became true migrants.

## Jung Sing County men migrate to New Zealand

A significant number from Jung-sing county (including Sun-gai village) came to New Zealand.  Well-known Sun-gai genealogist Ian Bing estimates that in the 1930's the population of Sun-gai was about 300 and that the number of people from Sun-gai village in New Zealand was about 100.

Even in the 1930's Sun-gai was "noted for the numerous overseas villagers now living abroad".  This migration away from Sun-gai has continued throughout this century such that there is now significantly more Sun-gai-ers living out of sun-gai than in the village itself.  Ian Bing estimates today that there is 1000 pop. in the village and maybe 2500 in New Zealand alone.

It is ironic that China has such a population crisis with burgeoning cities yet small villages such as Sun-gai becoming relatively empty.  Such that there are many "empty" unused ancestral homes.  These homes are caught in a time warp, left as they were, the day their owners left for "gum sarn".

The Jung Sing men that migrated to New Zealand came from a cluster of about 10 villages (including Sun-gai) in the south-west corner of Jung-sing County.  Few made it during the goldfield era.  The first significant group worked in the Forbury and Anderson's Bay market gardens (of Dunedin) circa 1880-90.

More Jung-sing men arrived later at or after the turn of the century but to the North island.  The Jung-sing men set up or worked in small business, usually market gardens and fruiterers!   Many of these men stayed in New Zealand.  Hence today's current Jung-sing Cantonese New Zealanders come from the same south-west corner of the county.  Sun-gai village has a big link to New Zealand.  It is our forefathers, those born in Sun-gai village, our fathers, grand or even great-grandfathers, that first came to New Zealand, that link us all at this Sun-gai gathering.

## Sun-gai Village

### The village

Sun-gai means "New Street".  It is one of a cluster of villages in the south-west corner of Jung-sing district that provided migrants to New Zealand.  The Rev. Alexander Don (Presbyterian Minister to the Chinese Miners of Otago and Southland) visited Sun-gai in 1898.  He was on very friendly terms with many of the Jung-sing men.  A quote from his excursion there:

![](images/sun-gai_village2_thumb.jpg) ![](images/sun-gai_village5_thumb.jpg)

_We visited the district whence go most of the Forbury and Anderson's Bay (Dunedin) gardeners and saw many old faces - in men returned from Dunedin...A busy place of 4,000 souls is Whitestone Mart (Bak-shek)), backed effectively by a luxuriously lichee-clad hill, from whose top one gets a good view of the villages whence go Chinese abroad.  Six miles straight north is the "south Scented (Larm-heung) Mount", about 3,500 feet high; a mile south is Toad Hill, overshadowing New-street village (Sun-gai) and with Under Stone (Shek-ha) hamlet at its very base; east are Sand Bank (Sha-t'au) and Melon Collar (Sic; Melon Mount/Hill) villages; west are White Water, Upper Shiu (Sheng-shiu/Upper Water) and Dam Bank; to the north-east is a rich flat, on the further side of which lies the great Tile-kiln Mart with its 15,000 inhabitants.  When market opens - every third day - the narrow paths converging here from all the surrounding villages are literally alive with strings of men, women and boys..'_

![](images/sun-gai_village1_thumb.jpg)

Rev. George McNeur, the first missionary to the Canton Villages Mission also visited Sun-gai and the adjacent villages of Jung-sing but in 1902.  From Sun-tong.he writes:

_'We had some difficulty in getting a boat to Sun-gai village.there were robbers about.That night I was waited upon by a deputation of New-street (Sun-gai) rats, that inquired loudly as to the nature of the eatables in my hamper.  Their tastes are so depraved that they actually tackled some Chinese cakes that were on the table.  Next morning my hands were all swollen with mosquito bites received the night before.'_

The bridge and fung-shui tower

Rev. McNeur took in the surrounding area.

_'when the sun set I rose and we climbed to the top of Toad Hill from which we surveyed the surrounding villages, canals and hills.'_

He took a photo of Sun-gai village from the top of Toad Hill and of the Sun-gai Bridge.  This bridge was later destroyed to try to stop the Japanese invasion.  This was in vain.  After the Japanese invasion the stone bridge was restored with steel.

He also photographed the 'new tower', which was described by a later missioner, Mr Mawson (in 1904) as the _"fung-shiu tower that looks down upon the bridge made so familiar by McNeur's photo"._   This tower or at least the bottom half still stands.  After communist "liberation", idol worshipping was frowned upon so that gradually all of the idols and religious artefacts within the tower were gradually destroyed.  Local county officials from Sa-to village had the tower destroyed in 1975.

![](images/sun-gai_village6_thumb.jpg) ![](images/sun-gai_village4_thumb.jpg)

### The Forts (Pao-lo)

There must have been a feud between neighbouring villages at this time too.

_'every half hour or so there is a "boom" of  a cannon and an answering "boom" always following close.  A village war over land boundaries has been going on for several years about three or four miles from here (Sun-gai village).  A great number of houses have been smashed and some lives lost through the cannonade.  I think, however that 99 out of 100 shots must be ineffective because while I was at New Street (Sun-gai) enough gunpowder was exploded to smash both villages to splinters.  The shock of the discharge can be felt quite distinctly here and the smoke can be seen.  Fancy this state of things going on for years and the Government without power to interfere, though fairly large vessels can sail within half a mile of the 'seat of war"._

 Banditry was also a problem at the turn of last century.  Such that Sun-gai village appealed to its overseas villagers for money circa, 1920.  this was to help restore law and order, buy weapons and to complete four forts to protect the village from bandit raids.

 It is rather ironic that it was in part the wealth that Sun-gai-errs had brought back to the village from overseas (gum sarn) that made banditry such a problem for the village.  It was the overseas Sun-gai-ers who were called on to finance defence. 

 Alan Wing (now retired in Hastings) recalls an attack on 'village uncle" Chan Doong Charm.

 _"Because sun-gai was a wealthy village in the early 1920's our village was attacked by an army of two to three hundred bandits.  Their target was to kidnap Chan Doong Charm, because of his wealth in Australia.  The night attack was well planned.  With a full moon, bandits had sentries posted at each entrance to the village. Unfortunately for the bandits Chan Doong Charm was a crack marksman with a rifle.  He took four firearms and ammunition.  He climbed onto the roof, shot the two sentries guarding one entrance and escaped.  Since then village elders decided to buy arms to protect the village, and with Chan Bo Hun in the army it was no problem.  The village purchased two Browning 50mm machine guns plus lots of small arms,  My brother Tom and Doong Charm escorted the 'goods" from Canton by train to the village and protected by troops from the army."_

 Forts were built to defend the village in the 1910-20's.  although only two forts (pao-lo) were built, two others were planned with the financial assistance of Chan Yuk-way, a Sun-gai-er then living in New Zealand. Unfortunately he died before the donation could be sent, hence the last two forts were not built.  The existing two forts lie on the west and eastern sides of the village (see panoramas: the western fort can be seen between the two ponds i.e. at the north end of jeurng fong pond and the south end of ye fong pond).  Harry yep recalls in the 19930's that the fort he visited as a boy was relatively new and that everyone had guns.  Even his grandmother had a gun in her home.  These were he said "troublesome times".

![](images/sun-gai_village3_thumb.jpg)

![](images/sun-gai_village7_thumb.jpg)
